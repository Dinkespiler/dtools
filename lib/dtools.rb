require_relative "./dtools/value_store.rb"

module DTools
  class Error < StandardError; end

  class Check
    class << self
      def blank_present?(data)
        value_store = ValueStore.new(data)
        value_store.blank_present?
      end

      def data_present?(data)
        value_store = ValueStore.new(data)
        value_store.data_present?
      end

      def blank_only?(data)
        value_store = ValueStore.new(data)
        value_store.blank_only?
      end

      def data_only?(data)
        value_store = ValueStore.new(data)
        value_store.data_only?
      end
    end
  end
end
