##
# 1. Description -
# This class takes data objects and outputs true if they are evaluated as blank
# and false otherwise.
#
# 2. Interface -
# this class has only one public method : #call
#
# 3. Parameters -
# the input data can be anything
#
# So far, no object is considered blank unless :
# - it's nil
# - it's a string comprised of only white spaces
# - it's an empty array
# - it's a hash without keys

class BlankValue
  class << self
    def call(value)
      case value.class.name
      when 'NilClass'
        true
      when 'String', 'Array', 'Hash'
        send("blank_#{value.class.name.downcase}?", value)
      else
        false
      end
    end

    private

    def blank_string?(value)
      value.gsub(' ', '') == ''
    end

    def blank_array?(value)
      value == []
    end

    def blank_hash?(value)
      value == {}
    end
  end
end
