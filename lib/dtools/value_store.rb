require_relative "./super_flatten.rb"
require_relative "./blank_value.rb"

class ValueStore
  attr_reader :values

  def initialize(data)
    @values = SuperFlatten.(data)
  end

  def blank_present?
    values.detect { |value| break true if BlankValue.(value) } || false
  end

  def data_present?
    values.detect { |value| break true unless BlankValue.(value) } || false
  end

  def blank_only?
    values.select { |value| !BlankValue.(value) }.empty?
  end

  def data_only?
    values.select { |value| BlankValue.(value) }.empty?
  end
end
