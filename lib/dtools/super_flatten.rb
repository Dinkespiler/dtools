##
# 1. Description -
# This class takes complex data objects and outputs an array of values
# where all the nested hashes and arrays have been flattened.
#
# 2. Interface -
# this class has only one public method : #call
#
# 3. Parameters -
# the input data can be anything

class SuperFlatten
  class << self
    def call(data)
      recursive_extraction_of_values [data]
    end

    private

    def recursive_extraction_of_values(array)
      array.each_with_object([]) do |element, values|
        new_values = extract_deeper?(element) ? deeper_extraction_of_values(element) : [element]
        values.concat(new_values)
      end
    end

    def extract_deeper?(element)
      element.is_a?(Array) || element.is_a?(Hash)
    end

    def deeper_extraction_of_values(element)
      values_only = element.is_a?(Hash) ? element.values : element
      recursive_extraction_of_values(values_only)
    end
  end
end
