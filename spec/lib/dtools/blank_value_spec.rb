
require 'spec_helper'

RSpec.describe BlankValue do
  describe '.call' do
    subject { described_class.(value) }

    context 'when the value is nil' do
      let(:value) { nil }

      it { expect(subject).to be true }
    end

    context 'when the value is a string containing no characters' do
      let(:value) { '' }

      it { expect(subject).to be true }
    end

    context 'when the value is a string containing only white spaces' do
      let(:value) { '   ' }

      it { expect(subject).to be true }
    end

    context 'when the value is a string containing any character except white spaces' do
      let(:value) { '   _ ' }

      it { expect(subject).to be false }
    end

    #.....
  end
end
