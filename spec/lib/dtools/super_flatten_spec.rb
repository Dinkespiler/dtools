require 'spec_helper'

RSpec.describe SuperFlatten do
  describe '.call' do
    subject { described_class.call(data) }

    context 'when data is an empty array' do
      let(:data) { [] }
      it 'returns an empty array' do
        expect(subject).to eq([])
      end
    end

    context 'when data is an empty hash' do
      let(:data) { {} }
      it 'returns an empty array' do
        expect(subject).to eq([])
      end
    end

    context 'when data is a string' do
      let(:data) { 'example' }
      it 'returns an array with the given string' do
        expect(subject).to eq(['example'])
      end
    end
  end
end
