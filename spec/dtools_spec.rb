require 'spec_helper'

RSpec.describe DTools do
  it 'has a version number' do
    expect(DTools::VERSION).not_to be nil
  end

  describe 'class Check' do
    subject { DTools::Check }

    let(:data) { 'data' }
    let(:value_store) { ValueStore.new(data) }

    describe 'method .blank_present?' do
      let(:call_method) { subject.blank_present?(data) }

      it 'instanciates a value store object and calls its method #blank_present?' do
        expect(ValueStore).to receive(:new).with(data).and_return(value_store)
        expect(value_store).to receive(:blank_present?)

        call_method
      end
    end

    describe 'method .data_present?' do
      let(:call_method) { subject.data_present?(data) }

      it 'instanciates a value store object and calls its method #data_present?' do
        expect(ValueStore).to receive(:new).with(data).and_return(value_store)
        expect(value_store).to receive(:data_present?)

        call_method
      end
    end

    describe 'method .blank_only?' do
      let(:call_method) { subject.blank_only?(data) }

      it 'instanciates a value store object and calls its method #blank_only?' do
        expect(ValueStore).to receive(:new).with(data).and_return(value_store)
        expect(value_store).to receive(:blank_only?)

        call_method
      end
    end

    describe 'method .data_only?' do
      let(:call_method) { subject.data_only?(data) }

      it 'instanciates a value store object and calls its method #data_only?' do
        expect(ValueStore).to receive(:new).with(data).and_return(value_store)
        expect(value_store).to receive(:data_only?)

        call_method
      end
    end
  end
end
